<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    public function index()
    {
        $users = User::when(request()->name, function($users) {
            $users = $users->where('name', 'like', '%'.request()->name.'%');
        })->latest()->paginate(5);

        return response()->json($users, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'mobile_phone' => 'required',
            'address' => 'required',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'mobile_phone' => $request->mobile_phone,
            'password' => $request->password,
            'address' => $request->address,
        ]);

        return response()->json([
           'data' => $user,
           'message' => 'Customer create successfully!'
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return response()->json($user, Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'customer not found'
            ], 404);
        }
        $user->update($request->only('name', 'email', 'mobile_phone', 'address', 'password'));

        return response()->json([
           'data' => $user,
           'message' => 'Customer update successfully!'
        ], Response::HTTP_ACCEPTED);
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'customer not found'
            ], 404);
        }

        $user->delete();

        return response()->json([
            'message' => 'Customer deleted successfully!'
        ], Response::HTTP_NO_CONTENT);
    }
}
