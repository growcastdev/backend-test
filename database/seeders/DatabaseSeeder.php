<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        UserSeeder::class;

        User::factory(5)->create();
        Product::factory(5)->create();
        Order::factory(5)->create();
        // OrderDetail::factory(5)->create();
    }
}
