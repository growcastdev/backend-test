<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $userId = $request->query('user_id');
        $orders = Order::getOrder();

        $orders->when($userId, function($query) use ($userId) {
            return $query->where('user_id', '=', $userId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $orders->paginate(5)
        ], Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'shipping_address' => 'required',
            'product_id' => 'required',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $order = Order::create([
            'user_id' => $request->user_id,
            'shipping_address' => $request->shipping_address,
        ]);

        $order->orderDetails()->create([
            'product_id' => $request->product_id,
            'quantity' => $request->quantity
        ]);

        return response()->json([
           'data' => $order,
           'message' => 'Order created successfully!'
        ], Response::HTTP_CREATED);
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if (!$order) {
            return response()->json([
                'status' => 'error',
                'message' => 'order not found'
            ], 404);
        }


        $order->update([
            'user_id' => $request->user_id,
            'shipping_address' => $request->shipping_address,
        ]);

        $orderDetail = OrderDetail::where('order_id', $order->id)->first();
        $orderDetail->update([
            'product_id' => $request->product_id,
            'quantity' => $request->quantity
        ]);

        return response()->json([
            'data' => $order,
            'message' => 'Order update successfully'
        ], Response::HTTP_ACCEPTED);
    }

    public function destroy($id)
    {
        $order = Order::find($id);

        if (!$order) {
            return response()->json([
                'status' => 'error',
                'message' => 'order not found'
            ], 404);
        }

        $order->delete();

        $orderDetail = OrderDetail::findOrFail($id);
        $orderDetail->delete();

        return response()->json([
            'message' => 'Order deleted successfully'
        ], Response::HTTP_NO_CONTENT);
    }
}
