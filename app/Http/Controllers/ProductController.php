<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')
                    ->paginate(5);

        return response()->json($products, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
            'description' => 'required',
            'image' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $product = Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'weight' => $request->weight,
            'description' => $request->description,
            'image' => $request->image,
        ]);

        return response()->json([
            'data' => $product,
            'message' => 'Product create successfully!'
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $product = Product::find($id);
        return response()->json($product, Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json([
                'status' => 'error',
                'message' => 'product not found'
            ], 404);
        }

        $product->update($request->only('name', 'price', 'weight', 'description', 'image'));

        return response()->json([
            'data' => $product,
            'message' => 'Product update successfully'
        ], Response::HTTP_ACCEPTED);
    }

    public function destroy($id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json([
                'status' => 'error',
                'message' => 'product not found'
            ], 404);
        }

        return response()->json([
            'message' => 'Product update successfully'
        ], Response::HTTP_NO_CONTENT);
    }
}
