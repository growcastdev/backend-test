<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'price' => $this->faker->numberBetween(50000, 500000),
            'weight' => $this->faker->numberBetween(5, 100),
            'description' => $this->faker->sentence(),
           'image' => $this->faker->imageUrl()
        ];
    }
}
