<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
    ];

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getOrder()
    {
        $order = Order::join('order_details', 'orders.id', '=', 'order_details.order_id')
                ->join('products', 'order_details.product_id', '=', 'products.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->select('orders.id', 'products.name AS product_name', 
                        'users.name AS user_name', 'users.id AS user_id', 'order_details.quantity', 
                        'orders.shipping_address', 'orders.created_at', 'orders.updated_at');

        return $order;
    }
}
